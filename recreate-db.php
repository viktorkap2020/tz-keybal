<?php
require_once __DIR__ . '/includes/db/DbDecorator.class.php';

$db = DbDecorator::getInstance();

$jsonData = json_decode(file_get_contents('Json/courses.json'), true);
echo "read " . count($jsonData) . " rows from JSON file<br>\n";

$db->exec("DROP TABLE IF EXISTS courses");

$sql = "CREATE TABLE courses (
    id INT AUTO_INCREMENT,
    currency VARCHAR(20),
    course DECIMAL(30, 15),
    PRIMARY KEY (id)
)";
$db->exec($sql);

foreach ($jsonData as $key => $value) {
    $insert = "INSERT INTO courses(currency, course) VALUES('{$key}', '{$value}');";
    $db->exec($insert);

}

echo "Database successfully recreated! Go to <a href='http://viktor2020-com-ua.1gb.ua/'>http://viktor2020-com-ua.1gb.ua/</a> to view table<br>\n";


