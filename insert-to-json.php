<?php
require_once __DIR__ . "/includes/functions.php";

$currency = $_POST['currency'];
$course = $_POST['course'];

$jsonData = json_decode(file_get_contents('Json/courses.json'), true);

$jsonData[$currency] = $course;


$jsonNew = json_encode($jsonData);
file_put_contents('Json/courses.json', $jsonNew);
redirect('/index.php');


