<?php
require_once __DIR__ . '/includes/db/DbDecorator.class.php';

$db = DbDecorator::getInstance();

$rows = $db->fetchAll("SELECT currency, course FROM courses");



printRowsAsTable($rows);

function printRowsAsTable(array $rows)
{

$result = "
<head>
<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css'>
  <script src='https://code.jquery.com/jquery-3.5.1.js'></script>
<script type='text/javascript' charset='utf8' src='https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js'></script>

<script>
    $(document).ready(function() {
    $('#example').DataTable({
        \"order\": [[ 1, \"desc\" ]]
    });
} );
</script>
</head>

" . getInsertForm('data.php', 'insert to db') . "
" . getInsertForm('insert-to-json.php', 'insert to json') . "

<table border='1' id='example'>
        <thead>
            <tr>
                <th>Currency</th>
                <th>Course</th>
            </tr>
        </thead>
";

    foreach ($rows as $key => $value) {
        $result .= "
<tr><td>{$value['currency']}</td><td>{$value['course']}</td></tr>";
    }
    $result .="</table>";

    echo $result;
}

function getInsertForm($action, $captionOnSubmitButton)
{
    return "
<form method='post' action='{$action}'>
<input name='currency' type='text' placeholder='currency'>
<input name='course' type='text' placeholder='course'>
<input type='submit' value='{$captionOnSubmitButton}'>
</form>";
}





