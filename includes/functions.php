<?php

function redirect(string $url)
{
    echo "<meta http-equiv=\"refresh\" content=\"0; url={$url}\">";
}